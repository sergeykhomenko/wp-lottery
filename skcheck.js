function checkform(){
        jQuery.ajax({
            type        : 'POST',
            url         : '/wp-content/plugins/wp-lottery/check.php',
            dataType    : 'html',
            data        : jQuery("#skcheck-form").serialize(),
            success     : function(response){
                    if(response == "WIN"){
                        jQuery("#skcheck-form-win").css("display","block");
                        jQuery("#skcheck-form-lose").css("display","none");
                    } else if(response == "LOSE"){
                        jQuery("#skcheck-form-win").css("display","none");
                        jQuery("#skcheck-form-lose").css("display","block");
                    }
                }
            });
}
