<?php
/*
Plugin Name: WP Lottery
Description: Проверка победителей лотореи
Version: 1.0
Author: Sergey Khomenko
Author URI: https://freelance.ru/sergeykhomenko/
Plugin URI: https://freelance.ru/sergeykhomenko/
*/

function skcheckActivation()
{
    global $wpdb;
    $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "right_codes(
        code VARCHAR(9),
        prize VARCHAR(35),
        PRIMARY KEY(code)
    );" );
    add_option('csv', 0);
}

function skcheckDeactivate()
{
    // do nothing
}


function skcheckImportCsv(
		$prefix,
		$rowData)
{
    $rowData = str_replace(",","','", $rowData);
    $q_import = "INSERT INTO " . $prefix . "right_codes VALUES ('" . $rowData . "'); ";

	return $q_import;
}

function skcheckAdminDash()
{
    global $wpdb;
    if (!empty($_REQUEST['csv-data'])){
        $csv_data = explode(",\r\n", $_REQUEST['csv-data']);
        foreach ($csv_data as $current_row) {
            if(!empty($current_row)){
                $wpdb->query(skcheckImportCsv($wpdb->prefix, $current_row));
            }
        }
        echo '<div id="message" class="updated fade"><p>Updated.</p></div>';
    }
    ?>
<div style="/*position:fixed;*/">
    <h1>SK Check Code Settings</h1>
    <p>Выберите файл с номерами и нажмите кнопку "Добавить номера" для того, чтобы добавить новые номера в базу</p>
    <form class="skcheck-file-loader" action="" method="post" enctype="multipart/form-data">
        <input type="file" id="fileElement" name="csv_file" style="width: 300px; height: 40px;" />
        <input type="submit" value="Добавить номера">
    <div id="fileData">

    </div>
    </form>
</div>
<script type="text/javascript">
var
    template = "<textarea name='csv-data' style='display:none;'>{{Data}}</textarea>",
    data = document.getElementById("fileData");

document.getElementById("fileElement").addEventListener("change", function(e){
    var file = this.files ? this.files[0] : {
        name: this.value
    }, fileReader = window.FileReader ? new FileReader() : null;

    if (file){
        if (fileReader){
            fileReader.addEventListener("loadend", function(e){
                data.innerHTML = template.replace("{{Name}}", file.name).replace("{{Size}}", file.size).replace("{{Data}}", e.target.result);
            }, false);
            fileReader.readAsText(file);
        } else {
            data.innerHTML = template.replace("{{Name}}", file.name).replace("{{Size}}", "Don't know").replace("{{Data}}", " ");
        }
    }
}, false);
</script>
    <?
}

function skcheckAdminMenuSetup()
{
    add_options_page('WP Lottery Settings', 'WP Lottery Settings', 8, __FILE__, 'skcheckAdminDash');
}

function skcheckfootertext(){
      echo "<script src=\"/wp-content/plugins/skcheck/skcheck.js\"></script>";
}




register_activation_hook(__FILE__, 'skcheckActivation');
register_deactivation_hook(__FILE__, 'skcheckDeactivate');
add_action('wp_footer', 'skcheckfootertext');
add_action('admin_menu', 'skcheckAdminMenuSetup');
//add_plugins_page('SK Check Code - Control Panel', 'SK Check Code', 'read', 'skcheck', 'skcheckAdmin');
?>
